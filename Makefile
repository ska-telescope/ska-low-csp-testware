# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) command to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c

CI_JOB_ID ?= local

########################################################################
# BASE
########################################################################

PROJECT = ska-low-csp-testware
DOCS_SPHINXOPTS ?= -W

include .make/base.mk

########################################################################
# PYTHON
########################################################################

PYTHON_SRC = src
PYTHON_TEST_FILE = tests/unit/
PYTHON_VARS_AFTER_PYTEST ?= --forked --disable-pytest-warnings

include .make/python.mk

mypy:
	$(PYTHON_RUNNER) mypy $(PYTHON_LINT_TARGET)

python-post-lint: mypy

########################################################################
# HELM
########################################################################

HELM_CHARTS_TO_PUBLISH ?= ska-low-csp-testware
HELM_CHARTS ?= $(HELM_CHARTS_TO_PUBLISH)

include .make/helm.mk

########################################################################
# K8S
########################################################################

CLUSTER_DOMAIN ?= cluster.local
EXPOSE_All_DS ?= false
KUBE_NAMESPACE ?= ska-low-csp-testware
MINIKUBE ?= true
TANGO_HOST ?= tango-databaseds:10000
TANGO_OPERATOR ?= true
TARANTA ?= false

K8S_CHART ?= test-parent
K8S_CHART_PARAMS = --set global.minikube=$(MINIKUBE) \
	--set global.exposeAllDS=$(EXPOSE_All_DS) \
	--set global.operator=$(TANGO_OPERATOR) \
	--set global.tango_host=$(TANGO_HOST) \
	--set global.cluster_domain=$(CLUSTER_DOMAIN) \
	--set ska-tango-taranta.enabled=$(TARANTA)
K8S_TEST_IMAGE_TO_TEST ?= artefact.skao.int/ska-low-csp-testware-test-runner:$(VERSION)
K8S_TEST_RUNNER_ADD_ARGS = --env=TANGO_HOST=$(TANGO_HOST)
# Override command used by k8s-test because it hard-codes the test path
# which includes the unit tests, which we don't want to run.
K8S_TEST_TEST_COMMAND ?= $(PYTHON_RUNNER) pytest --disable-pytest-warnings ./tests/integration | tee pytest.stdout
K8S_USE_HELMFILE ?= false

ifneq ($(CI_REGISTRY_IMAGE),)
MINIKUBE = false
K8S_CHART_PARAMS += --set ska-low-csp-testware.image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA) \
	--set ska-low-csp-testware.image.registry=$(CI_REGISTRY_IMAGE)
K8S_TEST_IMAGE_TO_TEST = $(CI_REGISTRY_IMAGE)/ska-low-csp-testware-test-runner:$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
endif

include .make/k8s.mk

########################################################################
# OCI
########################################################################

OCI_IMAGE_BUILD_CONTEXT = $(PWD)
OCI_IMAGES_TO_PUBLISH = ska-low-csp-testware

include .make/oci.mk
