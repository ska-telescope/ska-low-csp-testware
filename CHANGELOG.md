# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][keepachangelog], and this project adheres to [Semantic Versioning][semver].

## 0.0.1

Release date: 2025-02-04

### Added

- A new `PacketCapture` TANGO device that exposes commands to run `tcpdump` remotely on demand.

[keepachangelog]: https://keepachangelog.com/en/1.1.0/
[semver]: https://semver.org/spec/v2.0.0.html
