# pylint: disable=invalid-name, redefined-builtin
"""Release information for ska_low_csp_testware"""

import importlib.metadata

name = __package__
try:
    version = importlib.metadata.version(__package__)
except importlib.metadata.PackageNotFoundError:
    version = "unknown"
version_info = version.split(".")
description = "SKA LOW CSP Testware"
