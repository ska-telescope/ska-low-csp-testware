"""Utilities to help run tcpdump."""

import logging
import re
import signal
import subprocess
import threading
import time
from dataclasses import dataclass
from typing import Callable, TextIO

import psutil

__all__ = [
    "TcpdumpProcess",
    "TcpdumpStats",
    "TcpdumpStatsMonitor",
    "TcpdumpFailedToStartError",
]


class TcpdumpFailedToStartError(RuntimeError):
    """Error starting tcpdump process."""


class TcpdumpProcess:
    """
    Wrapper for a tcpdump process running in the background.
    """

    @classmethod
    def start(
        cls,
        *args: str,
        logger: logging.Logger | None = None,
    ):
        """
        Start a new tcpdump process.

        Calling this method will start tcpdump in the background with the
        provided arguments, and return an instance of
        :py:class:`TcpdumpProcess` representing the process handle.

        :param args: Command-line arguments passed to tcpdump
        :param logger: Python logger
        :raises TcpdumpFailedToStartError:
        :returns: :py:class:`TcpdumpProcess` instance
        """
        logger = logger or logging.getLogger(__name__)

        logger.debug("Starting tcpdump with args: %s", args)
        # Running tcpdup as a child process (so with shell=False and
        # start_new_session=False) made it impossible to stop it gracefully
        # because it does not seem to receive our SIGTERM.
        #
        # Hence we have to start a new session and wrap it in a shell.
        shell_handle = subprocess.Popen(  # pylint: disable=consider-using-with
            " ".join(["tcpdump", *args]),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            bufsize=1,
            shell=True,
            start_new_session=True,
            text=True,
        )

        # Give tcpdump a little time to initialise
        time.sleep(1)

        if shell_handle.poll() is not None:
            _, stderr = shell_handle.communicate()
            logger.error("tcpdump process failed to start: %s", stderr)
            raise TcpdumpFailedToStartError()

        logger.debug("Finding tcpdump process handle")
        process_handle = None
        for proc in psutil.Process(shell_handle.pid).children(recursive=True):
            if proc.name() == "tcpdump":
                logger.debug("tcpdump is running with pid %d", proc.pid)
                process_handle = proc

        if process_handle is None:
            logger.error(
                "tcpdump process did not start correctly, "
                "unable to find pid"
            )
            raise TcpdumpFailedToStartError()

        return cls(process_handle, shell_handle, logger=logger)

    def __init__(
        self,
        process_handle: psutil.Process,
        shell_handle: subprocess.Popen,
        logger: logging.Logger | None = None,
    ):
        self._process_handle = process_handle
        self._shell_handle = shell_handle
        self._logger = logger or logging.getLogger(__name__)

    @property
    def handle(self) -> psutil.Process:
        """Handle to the underlying process."""
        return self._process_handle

    @property
    def stdout(self) -> TextIO:
        """Handle to the process stdout stream."""
        return self._shell_handle.stdout  # type: ignore

    @property
    def stderr(self) -> TextIO:
        """Handle to the process stderr stream."""
        return self._shell_handle.stderr  # type: ignore

    def terminate(self, timeout: float = 10.0) -> int:
        """
        Terminate the running process.

        This will send SIGTERM to tcpdump and wait for it to terminate
        gracefully. If it is not terminated by the time the timeout expires,
        the process is terminated forcefully by sending SIGKILL.

        :param timeout: Time to wait for tcpdump to terminate gracefully
        :returns: Exit code of the subprocess.
        """
        if self._process_handle.is_running():
            self._logger.debug("Sending SIGTERM to tcpdump")
            self._process_handle.send_signal(signal.SIGTERM)

        exit_code = self._shell_handle.poll()
        if exit_code is None:
            self._logger.debug(
                "Waiting %d seconds for subprocess to exit", timeout
            )
            try:
                exit_code = self._shell_handle.wait(timeout=timeout)
            except subprocess.TimeoutExpired:
                self._logger.debug(
                    "Timeout expired waiting for subprocess to exit, killing."
                )
                self._shell_handle.kill()
                exit_code = 1

        self._logger.debug("Subprocess finished with exit code %d", exit_code)
        return exit_code


_packets_captured_pattern = re.compile(r"(\d+) packets captured")
_packets_received_pattern = re.compile(r"(\d+) packets received by filter")
_packets_dropped_pattern = re.compile(r"(\d+) packets dropped by kernel")


@dataclass
class TcpdumpStats:
    """Capture statistics from tcpdump."""

    packets_captured: int | None = None
    """The number of packets captured to the capture file."""

    packets_dropped: int | None = None
    """The number of packets dropped by the kernel."""

    packets_received: int | None = None
    """The number of packets received by the network interface."""

    @classmethod
    def parse_from_log(cls, log_line: str):
        """
        Parse capture statistics from tcpdump stderr output.

        :param log_line: A single line from the tcpdump stderr output
        :returns: :py:class:`TcpdumpStats` if the log line matched one of the
            patterns used to extract statistics, ``None`` otherwise.
        """
        captured: int | None = None
        dropped: int | None = None
        received: int | None = None

        if captured_match := _packets_captured_pattern.search(log_line):
            captured = int(captured_match.group(1))

        if received_match := _packets_received_pattern.search(log_line):
            received = int(received_match.group(1))

        if dropped_match := _packets_dropped_pattern.search(log_line):
            dropped = int(dropped_match.group(1))

        if captured is None and dropped is None and received is None:
            return None

        return cls(
            packets_captured=captured,
            packets_dropped=dropped,
            packets_received=received,
        )


class TcpdumpStatsMonitor:
    """
    Monitor capture statistics on a running tcpdump process.

    This monitor requests capture statistics from tcpdump by sending the
    SIGUSR1 signal. This triggers tcpdup to print a line to stderr reporting
    captured, received and dropped counts.

    Each line in the stderr output from tcpdump is parsed to extract the
    statistics, and the callback provided to the monitor is invoked for all
    successfully extracted stats.

    Note: to capture the final report printed by tcpdump when terminated,
    make sure to only stop the monitor after the tcpdump process has finished
    running.

    See: https://www.tcpdump.org/manpages/tcpdump.1.html
    """

    def __init__(
        self,
        callback: Callable[[TcpdumpStats], None],
        poll_interval: float = 1,
        logger: logging.Logger | None = None,
    ):
        self._callback = callback
        self._interval = poll_interval
        self._logger = logger or logging.getLogger(__name__)

        self._stop_event = threading.Event()
        self._process: TcpdumpProcess | None = None
        self._polling_thread: threading.Thread | None = None
        self._processing_thread: threading.Thread | None = None

    def start(self, tcpdump_process: TcpdumpProcess):
        """
        Start monitoring capture statistics from a tcpdump process.

        :param tcpdump_process: The process to monitor.
        """
        self._process = tcpdump_process
        self._polling_thread = threading.Thread(
            name="TcpdumpStatsMonitor-Polling",
            target=self._stats_polling_loop,
            daemon=True,
        )
        self._processing_thread = threading.Thread(
            name="TcpdumpStatsMonitor-Processing",
            target=self._process_stderr,
            args=(tcpdump_process.stderr,),
            daemon=True,
        )

        self._stop_event.clear()

        self._logger.debug("Start processing background thread")
        self._processing_thread.start()

        self._logger.debug("Start polling background thread")
        self._polling_thread.start()

    def stop(self):
        """
        Stop monitoring capture statistics from a tcpdump process.
        """

        self._logger.debug("Setting event to stop background threads")
        self._stop_event.set()

        for thread in [self._polling_thread, self._processing_thread]:
            if thread is not None:
                self._logger.debug(
                    "Waiting for %s thread to terminate",
                    thread.name,
                )
                thread.join()

        self._logger.debug("All background threads stopped")

        self._polling_thread = None
        self._processing_thread = None
        self._process = None

    def _stats_polling_loop(self):
        self._logger.debug("Start polling loop")

        while not self._stop_event.is_set():
            self._poll_stats()
            self._stop_event.wait(timeout=self._interval)

        self._logger.debug("Polling loop finished")

    def _poll_stats(self):
        if self._process is None or not self._process.handle.is_running():
            self._logger.warning(
                "Cannot request tcpdump stats: process not running"
            )
            self._stop_event.set()
            return

        self._logger.debug("Sending SIGUSR1 to tcpdump process")
        self._process.handle.send_signal(signal.SIGUSR1)

    def _process_stderr(self, stderr: TextIO):
        self._logger.debug("Start processing tcpdump stderr")

        for line in iter(stderr.readline, ""):
            line = line.strip()
            self._logger.debug("Processing tcpdump stderr line: %s", line)
            stats = TcpdumpStats.parse_from_log(line)
            if stats is None:
                continue

            self._logger.debug("Received stats: %s", stats)
            self._callback(stats)

        self._logger.debug("Finished processing tcpdump stderr")
