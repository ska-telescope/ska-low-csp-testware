"""
Module for the ``PacketCapture`` TANGO device.
"""

import os
import time
from typing import Any, cast

from scapy.all import get_if_addr, get_if_hwaddr, get_if_list
from ska_control_model import ResultCode
from ska_tango_base.base import SKABaseDevice
from ska_tango_base.commands import SlowDeviceInitCommand, SubmittedSlowCommand
from tango import AttReqType as AttrReqType
from tango import AttrQuality
from tango.server import attribute, command, device_property

from ska_low_csp_testware import release
from ska_low_csp_testware.packet_capture_component_manager import (
    CaptureOptions,
    PacketCaptureComponentManager,
)

# pylint: disable=protected-access
# pylint: disable=too-many-arguments
# pylint: disable=too-many-positional-arguments
# pylint: disable=too-many-instance-attributes

__all__ = ["PacketCapture", "main"]


class PacketCapture(SKABaseDevice[PacketCaptureComponentManager]):
    """
    TANGO device that allows remote capture of network packets.
    """

    interface: str = device_property(  # type: ignore
        mandatory=True,
        doc="""
The name of the network interface to capture packets on.

This is a mandatory property.
""",
    )

    default_bpf_filter: str = device_property(  # type: ignore
        default_value="",
        doc="""
Default value for the :py:meth:`bpf_filter` attribute.

This is an optional property, and will default to ``""`` if not set.
""",
    )

    default_min_packet_size: int = device_property(  # type: ignore
        default_value=0,
        doc="""
Default value for the :py:meth:`min_packet_size` attribute.

This is an optional property, and will default to ``0`` if not set.
""",
    )

    capture_dir: str = device_property(  # type: ignore
        default_value="/tmp",
        doc="""
Path to the directory where active captures are written to.

This is an optional property, and will default to ``/tmp`` if not set.

.. note:: Changing this property requires the device to re-initialise.
""",
    )

    storage_dir: str = device_property(  # type: ignore
        default_value="/tmp",
        doc="""
Path to the directory where capture files are moved to after
completion.

This is an optional property, and will default to ``/tmp`` if not set.

.. note:: Changing this property requires the device to re-initialise.
""",
    )

    attr_change_events = [
        "capture_stats__file_size",
        "capture_stats__packets_captured",
        "capture_stats__packets_dropped",
        "capture_stats__packets_received",
    ]

    def __init__(self, cl, name):
        self._capture_active = False
        self._capture_file_name = ""
        self._capture_stats__file_size = 0
        self._capture_stats__packets_captured = 0
        self._capture_stats__packets_dropped = 0
        self._capture_stats__packets_received = 0
        self._ip_address = ""
        self._mac_address = ""
        self._bpf_filter = ""
        self._min_packet_size = 0

        super().__init__(cl, name)

    def create_component_manager(self):
        return PacketCaptureComponentManager(
            capture_dir=self.capture_dir,
            storage_dir=self.storage_dir,
            logger=self.logger,
            communication_state_callback=self._communication_state_changed,
            component_state_callback=self._component_state_changed,
            capture_state_callback=self._update_capture_state,
        )

    class InitCommand(SlowDeviceInitCommand):  # type: ignore
        """Custom init command used to initialize this device."""

        def do(self, *args, **kwargs) -> tuple[ResultCode, str]:
            self.logger.info("Running custom init command")

            self._device._version_id = release.version
            self._device._build_state = (
                f"{release.name}, {release.version}, {release.description}"
            )

            self._device._bpf_filter = self._device.default_bpf_filter
            self._device._min_packet_size = (
                self._device.default_min_packet_size
            )

            for attr in self._device.attr_change_events:
                self._device.set_change_event(attr, True, False)

            self._device.admin_mode_model.perform_action("to_online")
            self._device.component_manager.start_communicating()

            iface = self._device.interface
            self.logger.info("Resolving network interface '%s'", iface)
            all_interfaces = get_if_list()
            if iface not in all_interfaces:
                status = (
                    f"Unknown network interface '{iface}', "
                    f"available interfaces: {', '.join(all_interfaces)}"
                )
                self.logger.error("Init failed: %s", status)
                self._device.op_state_model.perform_action("component_fault")
                self._device.set_status(status)
                self._completed()
                return ResultCode.FAILED, status

            iface_hwaddr = get_if_hwaddr(iface)
            self.logger.info(
                "Network interface %s has MAC address: %s", iface, iface_hwaddr
            )
            self._device._mac_address = iface_hwaddr

            iface_addr = get_if_addr(iface)
            self.logger.info(
                "Network interface %s has IP address: %s", iface, iface_addr
            )
            self._device._ip_address = iface_addr

            capture_dir = self._device.capture_dir
            if not os.path.exists(capture_dir):
                self.logger.info("Creating capture directory: %s", capture_dir)
                os.makedirs(capture_dir, exist_ok=True)

            storage_dir = self._device.storage_dir
            if not os.path.exists(storage_dir):
                self.logger.info("Creating storage directory: %s", storage_dir)
                os.makedirs(storage_dir, exist_ok=True)

            self._completed()
            status = "Device initialized OK"
            self.logger.info(status)
            return ResultCode.OK, status

    def init_command_objects(self):
        super().init_command_objects()
        for command_name, handler_name in [
            ("BeginCapture", "start_capture"),
            ("EndCapture", "stop_capture"),
        ]:
            self.register_command_object(
                command_name=command_name,
                command_object=SubmittedSlowCommand(
                    command_name=command_name,
                    command_tracker=self._command_tracker,
                    component_manager=self.component_manager,
                    method_name=handler_name,
                    logger=self.logger,
                ),
            )

    @attribute
    def ip_address(self) -> tuple[str, float, AttrQuality]:
        """
        The IP address of the configured network interface.
        """
        if not self._ip_address:
            return "", time.time(), AttrQuality.ATTR_INVALID

        return self._ip_address, time.time(), AttrQuality.ATTR_VALID

    @attribute
    def mac_address(self) -> tuple[str, float, AttrQuality]:
        """
        The MAC address of the configured network interface.
        """
        if not self._mac_address:
            return "", time.time(), AttrQuality.ATTR_INVALID

        return self._mac_address, time.time(), AttrQuality.ATTR_VALID

    @attribute
    def capture_active(self) -> bool:
        """
        Whether the device is currently capturing packets.
        """
        return self._capture_active

    @attribute
    def bpf_filter(self) -> str:
        """
        BPF filter applied by the kernel to filter which packets to capture.
        """
        return self._bpf_filter

    @bpf_filter.write  # type: ignore[no-redef]
    def bpf_filter(self, value: str):
        """
        Set the BPF filter.
        """
        self._bpf_filter = value
        self.logger.info("BPF filter set to '%s'", value)

    @bpf_filter.is_allowed
    def can_bpf_filter_be_changed(self, req_type: AttrReqType):
        """
        Check whether the ``bpf_filter`` attribute can be changed.

        Changing the BPF filter while a capture is active does not make sense,
        so this attribute can only be changed in between captures.
        """
        if req_type == AttrReqType.WRITE_REQ:
            return not self._capture_active

        return True

    @attribute
    def min_packet_size(self) -> int:
        """
        Minimum size of the packets to capture.
        All incoming packets smaller than the minimum size will be ignored.
        """
        return self._min_packet_size

    @min_packet_size.write  # type: ignore[no-redef]
    def min_packet_size(self, value: int):
        """
        Set the minimum packet size.
        """
        self._min_packet_size = value
        self.logger.info("Minimum packet size set to %d", value)

    @min_packet_size.is_allowed
    def can_min_packet_size_be_changed(self, req_type: AttrReqType):
        """
        Check whether the ``min_packet_size`` attribute can be changed.

        Changing the minimum packet size while a capture is active does not
        make sense, so this attribute can only be changed in between captures.
        """
        if req_type == AttrReqType.WRITE_REQ:
            return not self._capture_active

        return True

    @attribute
    def capture_file(self) -> str:
        """
        The name of the file where the packets are being captured to.
        """
        return self._capture_file_name

    @attribute
    def capture_stats__file_size(self) -> int:
        """
        The size of the capture file, in bytes.
        """
        return self._capture_stats__file_size

    @attribute
    def capture_stats__packets_received(self) -> int:
        """
        The total number of packets received.
        """
        return self._capture_stats__packets_received

    @attribute
    def capture_stats__packets_captured(self) -> int:
        """
        The total number of packets captured.
        """
        return self._capture_stats__packets_captured

    @attribute
    def capture_stats__packets_dropped(self) -> int:
        """
        The total number of packets dropped.
        """
        return self._capture_stats__packets_dropped

    @command(dtype_out="DevVarLongStringArray")
    def BeginCapture(  # pylint: disable=invalid-name
        self,
        capture_file_name: str,
    ) -> tuple[list[ResultCode], list[str]]:
        """
        Begin a new packet capture session.

        This will start a new background process that captures packets from
        the configured network interface to a capture file.

        While a capture session is active, the ``capture_stats__*`` attributes
        will automatically update to reflect the current state of the capture.

        This is a
        :doc:`long-running command
        <ska-tango-base:concepts/long-running-commands>`
        that returns as soon as the command is queued.

        :param capture_file_name: Name of the file where captured packets will
            be written to.

        :return: The :py:class:`ResultCode` and message.
        """
        capture_options = CaptureOptions(
            interface_name=self.interface,
            bpf_filter=self._bpf_filter,
            min_packet_size=self._min_packet_size,
        )
        handler = self.get_command_object("BeginCapture")
        result_code, message = handler(capture_file_name, capture_options)
        return ([result_code], [message])

    @command(dtype_out="DevVarLongStringArray")
    def EndCapture(  # pylint: disable=invalid-name
        self,
    ) -> tuple[list[ResultCode], list[str]]:
        """
        End the active packet capture session.

        This asynchronously terminates the background process and wait for it
        to exit.

        After the capture is finished, the capture file is automatically moved
        from the ``capture_dir`` to the ``storage_dir`` before this command
        is considered finished.

        This is a
        :doc:`long-running command
        <ska-tango-base:concepts/long-running-commands>`
        that returns as soon as the command is queued.

        :return: The :py:class:`ResultCode` and message.
        """
        handler = self.get_command_object("EndCapture")
        result_code, message = handler()
        return ([result_code], [message])

    def _update_capture_state(
        self,
        capture_active: bool | None = None,
        capture_file_name: str | None = None,
        file_size: int | None = None,
        packets_captured: int | None = None,
        packets_received: int | None = None,
        packets_dropped: int | None = None,
    ):
        if capture_active is not None:
            self._update_attr("capture_active", capture_active)

        if capture_file_name is not None:
            self._update_attr("capture_file_name", capture_file_name)

        if file_size is not None:
            self._update_attr("capture_stats__file_size", file_size)

        if packets_captured is not None:
            self._update_attr(
                "capture_stats__packets_captured", packets_captured
            )

        if packets_dropped is not None:
            self._update_attr(
                "capture_stats__packets_dropped", packets_dropped
            )

        if packets_received is not None:
            self._update_attr(
                "capture_stats__packets_received", packets_received
            )

    def _update_attr(self, attr_name: str, attr_value: Any):
        prev_value = getattr(self, f"_{attr_name}")
        setattr(self, f"_{attr_name}", attr_value)

        if attr_value != prev_value and attr_name in self.attr_change_events:
            self.logger.debug(
                "Pushing new change event for attribute '%s' with value: %s",
                attr_name,
                attr_value,
            )
            self.push_change_event(attr_name, attr_value)


def main(*args: str, **kwargs: str) -> int:
    """
    Entry point for module.

    :param args: positional arguments
    :param kwargs: named arguments

    :return: exit code
    """
    return cast(
        int,
        PacketCapture.run_server(args=args or None, **kwargs),
    )


if __name__ == "__main__":
    main()
