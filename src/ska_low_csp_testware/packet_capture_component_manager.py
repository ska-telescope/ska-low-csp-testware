"""Packet capture component manager."""

import logging
import os
import shutil
import threading
from dataclasses import dataclass
from typing import Callable, Protocol

import backoff
from ska_control_model import (
    CommunicationStatus,
    PowerState,
    ResultCode,
    TaskStatus,
)
from ska_tango_base.base import (
    CommunicationStatusCallbackType,
    TaskCallbackType,
)
from ska_tango_base.executor import TaskExecutorComponentManager
from watchdog.events import (
    DirCreatedEvent,
    DirModifiedEvent,
    FileClosedEvent,
    FileCreatedEvent,
    FileModifiedEvent,
    FileSystemEventHandler,
)
from watchdog.observers import Observer
from watchdog.observers.api import BaseObserver

from ska_low_csp_testware.tcpdump import (
    TcpdumpFailedToStartError,
    TcpdumpProcess,
    TcpdumpStats,
    TcpdumpStatsMonitor,
)

# pylint: disable=too-few-public-methods
# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-arguments
# pylint: disable=too-many-positional-arguments

__all__ = [
    "CaptureOptions",
    "PacketCaptureComponentManager",
]


class CaptureFileMonitor(FileSystemEventHandler):
    """Propagate file system events on the capture file."""

    def __init__(
        self,
        callback: Callable[[os.stat_result], None],
        logger: logging.Logger | None = None,
    ):
        self._callback = callback
        self._logger = logger or logging.getLogger(__name__)

        self._observer: BaseObserver | None = None

    def start(self, capture_file_path: str):
        """
        Start the monitor.

        :param capture_file_path: Path to the capture file
        """
        self._logger.debug(
            "Starting capture file monitor with file %s", capture_file_path
        )
        self._observer = Observer()
        self._observer.schedule(self, capture_file_path)

        try:
            self._try_start(self._observer)
        except OSError:
            self._logger.exception("Failed to start file observer")
            raise

        self._logger.debug("Capture file monitor started")

    @backoff.on_exception(backoff.constant, OSError, max_tries=3, interval=1)
    def _try_start(self, observer: BaseObserver):
        self._logger.debug("Attempting to start file observer")
        observer.start()

    def stop(self):
        """
        Stop the monitor.
        """
        self._logger.debug("Stopping capture file monitor")

        if self._observer is not None and self._observer.is_alive():
            self._logger.debug("Stopping observer thread")
            self._observer.stop()
            self._logger.debug("Waiting for observer to terminate")
            self._observer.join()

        self._logger.debug("Capture file monitor stopped")

        self._observer = None

    def on_created(self, event: DirCreatedEvent | FileCreatedEvent) -> None:
        if event.is_directory:
            return

        self._logger.debug("File created: %s", event.src_path)
        self._update_file_info(event.src_path)

    def on_closed(self, event: FileClosedEvent) -> None:
        self._logger.debug("File closed: %s", event.src_path)
        self._update_file_info(event.src_path)

    def on_modified(self, event: DirModifiedEvent | FileModifiedEvent) -> None:
        if event.is_directory:
            return

        self._logger.debug("File modified: %s", event.src_path)
        self._update_file_info(event.src_path)

    def _update_file_info(self, file_path: bytes | str):
        self._logger.debug("Updating file info for %s", file_path)
        self._callback(os.stat(file_path))


class CaptureStateCallback(Protocol):
    """Protocol for the capture state callback function."""

    def __call__(
        self,
        capture_active: bool | None = None,
        capture_file_name: str | None = None,
        file_size: int | None = None,
        packets_captured: int | None = None,
        packets_received: int | None = None,
        packets_dropped: int | None = None,
    ) -> None:
        """
        Invoked when the capture state changes.
        """


@dataclass
class CaptureOptions:
    """Options used to configure tcpdump."""

    interface_name: str
    bpf_filter: str
    min_packet_size: int


class PacketCaptureComponentManager(TaskExecutorComponentManager):
    """Packet capture component manager."""

    def __init__(
        self,
        capture_dir: str,
        storage_dir: str,
        logger: logging.Logger,
        communication_state_callback: CommunicationStatusCallbackType,
        component_state_callback: Callable[..., None],
        capture_state_callback: CaptureStateCallback,
        *args,
        **kwargs,
    ):
        super().__init__(
            logger,
            communication_state_callback,
            component_state_callback,
            *args,
            power=PowerState.UNKNOWN,
            fault=None,
            **kwargs,
        )

        self._capture_dir = capture_dir
        self._storage_dir = storage_dir
        self._capture_state_callback = capture_state_callback

        self._tcpdump_stats_monitor = TcpdumpStatsMonitor(
            callback=self._on_tcpdump_stats,
            poll_interval=1.0,
            logger=logger,
        )

        self._capture_file_monitor = CaptureFileMonitor(
            callback=self._on_capture_file_change,
            logger=logger,
        )

        self._capture_file_name = ""
        self._capture_file_size = 0
        self._capture_packet_count = 0
        self._tcpdump_process: TcpdumpProcess | None = None

    def start_communicating(self):
        self._update_communication_state(CommunicationStatus.ESTABLISHED)
        self._update_component_state(power=PowerState.ON, fault=False)

    def stop_communicating(self):
        self._update_component_state(power=PowerState.OFF, fault=None)
        self._update_communication_state(CommunicationStatus.DISABLED)

    def on(self, task_callback: TaskCallbackType | None = None):
        self.logger.debug("Rejecting On command")
        return TaskStatus.REJECTED, "Operation not supported"

    def off(self, task_callback: TaskCallbackType | None = None):
        self.logger.debug("Rejecting Off command")
        return TaskStatus.REJECTED, "Operation not supported"

    def standby(self, task_callback: TaskCallbackType | None = None):
        self.logger.debug("Rejecting Standby command")
        return TaskStatus.REJECTED, "Operation not supported"

    def reset(self, task_callback: TaskCallbackType | None = None):
        self.logger.debug("Rejecting Reset command")
        return TaskStatus.REJECTED, "Operation not supported"

    def start_capture(
        self,
        capture_file_name: str,
        capture_options: CaptureOptions,
        task_callback: TaskCallbackType | None = None,
    ):
        """
        Submit a new task to start a capture.

        This method returns as soon as the task is submitted.
        Starting a new capture while another capture is already running
        is not allowed.

        :param interface_name: Name of the network interface to capture on.
        :param capture_file_name: Name of the capture file.
        :param task_callback: Callback used to signal task progress.
        """
        return self.submit_task(
            self._start_capture_task,
            args=(capture_file_name, capture_options),
            is_cmd_allowed=lambda: self._tcpdump_process is None
            or not self._tcpdump_process.handle.is_running(),
            task_callback=task_callback,
        )

    def _start_capture_task(
        self,
        capture_file_name: str,
        capture_options: CaptureOptions,
        task_callback: TaskCallbackType,
        task_abort_event: threading.Event,  # pylint: disable=unused-argument
    ):
        capture_file_path = os.path.join(self._capture_dir, capture_file_name)
        self.logger.info(
            "Start capturing packets from interface '%s' to %s",
            capture_options.interface_name,
            capture_file_path,
        )
        task_callback(status=TaskStatus.IN_PROGRESS)

        tcpdump_args = [
            f"--interface={capture_options.interface_name}",
            f"--snapshot-length={capture_options.min_packet_size}",
            "--buffer-size=919400",
            "--no-promiscuous-mode",
            "-n",
            "-w",
            capture_file_path,
            capture_options.bpf_filter,
        ]

        try:
            tcpdump_process = TcpdumpProcess.start(
                *tcpdump_args,
                logger=self.logger,
            )
        except TcpdumpFailedToStartError:
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(
                    ResultCode.FAILED,
                    "Failed to start packet capture process",
                ),
            )
            return

        self._tcpdump_process = tcpdump_process
        self._capture_file_name = capture_file_name
        self._capture_state_callback(
            capture_active=True,
            capture_file_name=capture_file_name,
            file_size=0,
            packets_captured=0,
            packets_received=0,
            packets_dropped=0,
        )

        self._tcpdump_stats_monitor.start(tcpdump_process)
        self._capture_file_monitor.start(capture_file_path)

        self.logger.info("Packet capture started")
        task_callback(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Packet capture started"),
        )

    def stop_capture(self, task_callback: TaskCallbackType | None = None):
        """
        Submit a new task to stop a running capture.

        This method returns as soon as the task is submitted.
        Stopping a capture is not allowed if we don't have a handle
        to a running process.

        :param task_callback: Callback used to signal task progress.
        """
        return self.submit_task(
            self._stop_capture_task,
            is_cmd_allowed=lambda: self._tcpdump_process is not None,
            task_callback=task_callback,
        )

    def _stop_capture_task(
        self,
        task_callback: TaskCallbackType,
        task_abort_event: threading.Event,  # pylint: disable=unused-argument
    ):
        self.logger.info("Stopping packet capture")
        task_callback(status=TaskStatus.IN_PROGRESS)

        if self._tcpdump_process is not None:
            _ = self._tcpdump_process.terminate()

        self._tcpdump_stats_monitor.stop()
        self._capture_file_monitor.stop()

        self._tcpdump_process = None
        self._capture_state_callback(
            capture_active=False,
        )

        if self._capture_dir != self._storage_dir:
            self.logger.info("Moving capture file to storage directory")
            capture_file_path = os.path.join(
                self._capture_dir, self._capture_file_name
            )
            storage_file_path = os.path.join(
                self._storage_dir, self._capture_file_name
            )
            shutil.move(capture_file_path, storage_file_path)
            self.logger.debug(
                "Finished moving %s to %s",
                capture_file_path,
                storage_file_path,
            )

        self.logger.info(
            "Finished capturing %d packets for a total size of %d bytes",
            self._capture_packet_count,
            self._capture_file_size,
        )
        self._capture_file_size = 0
        self._capture_packet_count = 0

        task_callback(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Capture stopped"),
        )

    def _on_tcpdump_stats(self, stats: TcpdumpStats):
        self.logger.debug(
            "Invoking capture state callback with stats: %s", stats
        )
        self._capture_state_callback(
            packets_captured=stats.packets_captured,
            packets_dropped=stats.packets_dropped,
            packets_received=stats.packets_received,
        )

        if stats.packets_captured is not None:
            self._capture_packet_count = stats.packets_captured

    def _on_capture_file_change(self, file_info: os.stat_result):
        file_size = file_info.st_size
        self.logger.debug(
            "Invoking capture state callback with file size: %d", file_size
        )
        self._capture_state_callback(
            file_size=file_size,
        )

        self._capture_file_size = file_size

    def _on_unhandled_exception(self, exception: Exception):
        self.logger.error("Unhandled exception occurred: %s", exception)
        self._update_component_state(fault=True)
