# SKA LOW-CSP Testware

This repository contains testware used for the integration and verification of the LOW-CSP in the Digital Signal PSI.

## User documentation

User documentation for this project can be found here: https://developer.skao.int/projects/ska-low-csp-testware/en/latest/

## Developer instructions

### Prerequisites

This project requires the following to be installed on the development machine:

- Python 3.10
- Poetry 1.8
- Make

Additionally, to build and deploy the OCI images and/or Helm charts:

- Docker or Podman
- Kubectl
- Helm

### Cloning the repository

To clone the repository with all of its submodules:

    git clone --recursive git@gitlab.com:ska-telescope/ska-low-csp-testware.git

### Set up Poetry environment

To set up a local virtual environment and install the Python dependencies:

    poetry install

To activate the virtual environment:

    poetry shell

> [!NOTE]
> All subsequent `make` targets in this README assume that they are run from within the virtual environment.

### Python development

#### Formatting and linting

This repository aims to follow the Python coding standards used by the SKAO, and therefore requires that the code is formatted and linted properly.

Additionally, this project uses `mypy` for static type checking.

To auto-format the Python code according to the standards, run:

    make python-format

To lint and type-check the Python code, run:

    make python-lint

#### Unit testing

Unit tests for this project can be found in [`tests/unit/`](./tests/unit/), and can be run with:

    make python-test

#### Integration testing

Integration tests for this project can be found in [`tests/integration/`](./tests/integration/),
and are typically run in GitLab CI/CD pipelines as part of the `k8s-test` job.

### Documentation

The user documentation is built using Sphinx and can be found in [`docs/src/`](./docs/src/).

To build the documentation, run:

    make docs-build html

Note that documentation builds are incremental, to perform a full documentation build from scratch first clean the build directory with:

    make docs-build clean

## License information

See [LICENSE](./LICENSE).

## Support

For questions or remarks related to this project, contact [#team-topic](https://skao.slack.com/archives/C05CZKCM22U).
