"""Utility functions."""

import logging
import threading
from typing import Any

import tango
from ska_control_model import ResultCode, TaskStatus
from ska_tango_base.base.base_component_manager import JSONData
from ska_tango_base.faults import CommandError
from ska_tango_base.long_running_commands_api import invoke_lrc

__all__ = [
    "invoke_and_wait_for_lrc",
]


def invoke_and_wait_for_lrc(
    device: tango.DeviceProxy,
    command: str,
    command_args: tuple[Any] | None = None,
    timeout_s: int = 10,
    logger: logging.Logger | None = None,
) -> ResultCode:
    """Invoke a long-running command and wait for it to complete."""
    logger = logger or logging.getLogger("ska_low_csp_testware.tests")

    done = threading.Event()
    results = []

    # We must accept unknown kwargs here as future versions of invoke_lrc
    # may pass additional arguments
    def lrc_callback(  # pylint: disable=unused-argument
        status: TaskStatus | None = None,
        progress: int | None = None,
        result: JSONData | None = None,
        error: tuple[Any] | None = None,
        **kwargs,
    ):
        if result is None:
            return

        if isinstance(result, int):
            results.append(result)

        if isinstance(result, (list, tuple)):
            for r in result:
                if isinstance(r, int):
                    results.append(r)

        if status in {
            TaskStatus.ABORTED,
            TaskStatus.COMPLETED,
            TaskStatus.FAILED,
            TaskStatus.NOT_FOUND,
            TaskStatus.STAGING,
        }:
            done.set()

    logger.debug("Invoking LRC '%s' with args %s", command, command_args)
    try:
        # We have to keep this lrc_subscriptions alive for as long as we are
        # interested in this LRC
        lrc_subscriptions = (  # noqa: F841, pylint: disable=unused-variable
            invoke_lrc(
                lrc_callback,
                device,
                command=command,
                command_args=command_args,
                logger=logger,
            )
        )
    except (CommandError, tango.DevFailed) as err:
        logger.error("LRC failed: %s", err)
        return ResultCode.FAILED

    if not done.wait(timeout=timeout_s):
        logger.error("LRC timed out after %d seconds", timeout_s)
        return ResultCode.FAILED

    # result has been populated here as done is set
    result = ResultCode(results[0])
    logger.debug("LRC completed with result %s", result)
    return result
