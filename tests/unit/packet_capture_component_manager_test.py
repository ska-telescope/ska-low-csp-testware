# pylint: disable=missing-module-docstring
# pylint: disable=missing-function-docstring

import pathlib
from typing import Type

import pytest
from ska_tango_testing.mock import MockCallable
from watchdog.events import (
    FileClosedEvent,
    FileCreatedEvent,
    FileModifiedEvent,
    FileSystemEvent,
)

from ska_low_csp_testware.packet_capture_component_manager import (
    CaptureFileMonitor,
)


@pytest.fixture(name="file")
def fxt_file(tmp_path: pathlib.Path):
    file = tmp_path / "capturefile"
    file.write_text("capture capture capture")
    return file


@pytest.mark.parametrize(
    "event",
    [
        FileCreatedEvent,
        FileModifiedEvent,
        FileClosedEvent,
    ],
)
def test_capture_file_monitor_updates_file_size_on_event(
    file: pathlib.Path,
    event: Type[FileSystemEvent],
):
    expected = file.stat()

    callback = MockCallable()
    sut = CaptureFileMonitor(callback=callback)
    sut.dispatch(event(str(file)))

    callback.assert_call(expected)
