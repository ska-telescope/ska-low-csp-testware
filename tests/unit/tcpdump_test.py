# pylint: disable=missing-module-docstring

from ska_low_csp_testware.tcpdump import TcpdumpStats


def test_tcpdump_stats_extracts_stats_from_a_single_line():
    """
    Tests whether the :py:class:`TcpdumpStats` processes statistics from a
    single line correctly. This matches the format used by tcpdump when
    requesting statistics while the capture is active.
    """
    line = (
        "tcpdump: 1234 packets captured, "
        "9999 packets received by filter, "
        "42 packets dropped by kernel"
    )
    expected = TcpdumpStats(
        packets_captured=1234,
        packets_dropped=42,
        packets_received=9999,
    )
    actual = TcpdumpStats.parse_from_log(line)

    assert actual == expected


def test_tcpdump_stats_extracts_stats_from_multiple_lines():
    """
    Tests whether the :py:class:`TcpdumpStats` processes statistics from
    multiple lines correctly. This matches the format used by tcpdump when the
    process is terminated, and it prints a final report.
    """
    lines = """\
1234 packets captured
9999 packets received by filter
42 packets dropped by kernel
"""
    expected = [
        TcpdumpStats(packets_captured=1234),
        TcpdumpStats(packets_received=9999),
        TcpdumpStats(packets_dropped=42),
    ]

    actual = list(map(TcpdumpStats.parse_from_log, lines.splitlines()))

    assert actual == expected
