# pylint: disable=missing-module-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=missing-class-docstring

import contextlib
import pathlib

import pytest
from scapy.all import get_if_list
from ska_control_model import AdminMode, ResultCode
from tango import AttrQuality, DeviceProxy, DevState
from tango.test_context import DeviceTestContext

from ska_low_csp_testware.packet_capture_device import PacketCapture


@contextlib.contextmanager
def create_device(
    interface: str | None = None,
    default_bpf_filter: str | None = None,
    default_min_packet_size: int | None = None,
    capture_dir: str | None = None,
    storage_dir: str | None = None,
):
    device_properties = {
        "interface": interface or get_if_list()[0],
        "default_bpf_filter": default_bpf_filter,
        "default_min_packet_size": default_min_packet_size,
        "capture_dir": capture_dir,
        "storage_dir": storage_dir,
    }

    with DeviceTestContext(
        PacketCapture,
        properties=dict(
            filter(
                lambda item: item[1] is not None,
                device_properties.items(),
            )
        ),
    ) as device:
        yield device


@pytest.fixture(name="device")
def fxt_device():
    with create_device() as device:
        yield device


def test_device_inits_ok(device: DeviceProxy):
    assert device.adminMode == AdminMode.ONLINE
    assert device.State() == DevState.ON


def test_device_inits_ip_address_attr(device: DeviceProxy):
    ip_address_attr = device.read_attribute("ip_address")
    assert ip_address_attr.quality == AttrQuality.ATTR_VALID
    assert ip_address_attr.value is not None


def test_device_inits_mac_address_attr(device: DeviceProxy):
    mac_address_attr = device.read_attribute("mac_address")
    assert mac_address_attr.quality == AttrQuality.ATTR_VALID
    assert mac_address_attr.value is not None


def test_device_init_creates_capture_dir_if_not_exists(
    tmp_path: pathlib.Path,
):
    capture_dir = tmp_path / "captures"
    assert capture_dir.exists() is False

    with create_device(capture_dir=str(capture_dir)):
        assert capture_dir.exists() is True


def test_device_init_creates_storage_dir_if_not_exists(
    tmp_path: pathlib.Path,
):
    storage_dir = tmp_path / "storage"
    assert storage_dir.exists() is False

    with create_device(storage_dir=str(storage_dir)):
        assert storage_dir.exists() is True


def test_device_can_write_bpf_filter(device: DeviceProxy):
    device.bpf_filter = "udp"
    assert device.bpf_filter == "udp"


def test_device_can_write_min_packet_size(device: DeviceProxy):
    device.min_packet_size = 100
    assert device.min_packet_size == 100


def test_device_with_invalid_interface_inits_nok():
    with create_device(interface="no-such-interface") as device:
        assert device.State() == DevState.FAULT
        assert (
            device.read_attribute("mac_address").quality
            == AttrQuality.ATTR_INVALID
        )
        assert (
            device.read_attribute("ip_address").quality
            == AttrQuality.ATTR_INVALID
        )


def test_device_sets_bpf_filter_from_properties():
    bpf_filter = "udp"

    with create_device(
        default_bpf_filter=bpf_filter,
    ) as device:
        assert device.bpf_filter == bpf_filter


def test_device_sets_min_packet_size_from_properties():
    min_packet_size = 100

    with create_device(
        default_min_packet_size=min_packet_size,
    ) as device:
        assert device.min_packet_size == min_packet_size


@pytest.mark.parametrize(
    "attr_name",
    [
        "capture_active",
        "capture_file",
        "capture_stats__file_size",
        "capture_stats__packets_captured",
        "capture_stats__packets_dropped",
        "capture_stats__packets_received",
    ],
)
def test_device_attribute_reads_ok(
    device: DeviceProxy,
    attr_name: str,
):
    attr = device.read_attribute(attr_name)
    assert attr.quality == AttrQuality.ATTR_VALID


@pytest.mark.parametrize(
    "command_name",
    [
        "On",
        "Off",
        "Standby",
        "Reset",
    ],
)
def test_device_rejects_command(
    device: DeviceProxy,
    command_name: str,
):
    [[result_code], _] = device.command_inout(command_name)
    assert result_code == ResultCode.REJECTED
