# pylint: disable=missing-module-docstring

import time

import tango
from assertpy import assert_that
from ska_tango_testing.integration import TangoEventTracer

from ..utils import invoke_and_wait_for_lrc


def test_packet_capture(
    packet_capture_device: tango.DeviceProxy,
    event_tracer: TangoEventTracer,
):
    """
    Verify that a capture can be performed without running into errors.

    The system under test in a CI environment does not give us the ability
    to actually send some packets for the device to capture, so the only thing
    it will capture is whatever traffic is sent to the pod.

    We therefore can't make any direct assertions about the statistics,
    only that they should be "not zero" at the end of the test.
    """
    capture_file_name = "test.pcap"

    expected_change_events = [
        "capture_stats__file_size",
        "capture_stats__packets_received",
        "capture_stats__packets_captured",
    ]

    for attr in expected_change_events:
        event_tracer.subscribe_event(packet_capture_device, attr)

    invoke_and_wait_for_lrc(
        packet_capture_device, "BeginCapture", (capture_file_name,)
    )

    assert packet_capture_device.capture_file == capture_file_name
    assert packet_capture_device.capture_active is True

    time.sleep(5)

    assert packet_capture_device.capture_stats__file_size > 0
    assert packet_capture_device.capture_stats__packets_received > 0
    assert packet_capture_device.capture_stats__packets_captured > 0

    invoke_and_wait_for_lrc(packet_capture_device, "EndCapture")

    assert packet_capture_device.capture_active is False

    for attr in expected_change_events:
        assert_that(event_tracer).has_change_event_occurred(
            device_name=packet_capture_device,
            attribute_name=attr,
            custom_matcher=lambda e: e.attribute_value > 0,
        )
