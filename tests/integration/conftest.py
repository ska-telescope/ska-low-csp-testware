# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring

import pytest
import tango
from ska_tango_testing.integration import TangoEventTracer


@pytest.fixture(name="packet_capture_device")
def fxt_packet_capture_device():
    return tango.DeviceProxy("testware/packet-capture/1")


@pytest.fixture(name="event_tracer")
def fxt_event_tracer():
    return TangoEventTracer()
