=============
PacketCapture
=============

The PacketCapture TANGO device can be used to remotely capture all traffic sent to it,
and write the captured packets to a file.

Commands
--------

.. automethod:: ska_low_csp_testware.packet_capture_device::PacketCapture.BeginCapture
.. automethod:: ska_low_csp_testware.packet_capture_device::PacketCapture.EndCapture

Attributes
----------

Read/Write Attributes
^^^^^^^^^^^^^^^^^^^^^

.. autoattribute:: ska_low_csp_testware.packet_capture_device::PacketCapture.bpf_filter
.. autoattribute:: ska_low_csp_testware.packet_capture_device::PacketCapture.min_packet_size

Read-Only Attributes
^^^^^^^^^^^^^^^^^^^^

.. autoattribute:: ska_low_csp_testware.packet_capture_device::PacketCapture.ip_address
.. autoattribute:: ska_low_csp_testware.packet_capture_device::PacketCapture.mac_address
.. autoattribute:: ska_low_csp_testware.packet_capture_device::PacketCapture.capture_active
.. autoattribute:: ska_low_csp_testware.packet_capture_device::PacketCapture.capture_file
.. autoattribute:: ska_low_csp_testware.packet_capture_device::PacketCapture.capture_stats__file_size
.. autoattribute:: ska_low_csp_testware.packet_capture_device::PacketCapture.capture_stats__packets_received
.. autoattribute:: ska_low_csp_testware.packet_capture_device::PacketCapture.capture_stats__packets_captured
.. autoattribute:: ska_low_csp_testware.packet_capture_device::PacketCapture.capture_stats__packets_dropped

Properties
----------

.. autoattribute:: ska_low_csp_testware.packet_capture_device::PacketCapture.interface
.. autoattribute:: ska_low_csp_testware.packet_capture_device::PacketCapture.default_bpf_filter
.. autoattribute:: ska_low_csp_testware.packet_capture_device::PacketCapture.default_min_packet_size
.. autoattribute:: ska_low_csp_testware.packet_capture_device::PacketCapture.capture_dir
.. autoattribute:: ska_low_csp_testware.packet_capture_device::PacketCapture.storage_dir
